#!/bin/bash
# ./curl-create-cookie.sh 10 http://www.ste-magnificient.be
nbr_curl=$1
url_curl=$2
nbr_init=0
while [ $nbr_init -lt $nbr_curl ]
do
        curl -k -c cookies.txt $url_curl
        nbr_init=`expr $nbr_init + 1`
done
