# bash-tools-script
## Description
Ce repos contiendra des scripts bash tools pour mes différents projets

### ncurl.sh
Ce script sert à effectuer un nombre de curl par vous sur une url donnée. 

Exemple 
Je souhaite effectuer 10 curl sur hello.domain.tld

```bash=
./ncurl.sh 10 https://hello.domain.tld
```
### ncurl-create-cookie.sh
Ce script effectue la même tâche que ci dessus mais permet de créer un cookie local. 

Exemple d'utilisation
Dans un contexte ou vous travaillerez avec des sticky session. 


## Author
Steve Decot
